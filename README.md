Clone project with command
### `git clone https://gitlab.com/karpenko291999/note-web-app.git`

In the project directory, install all dependencies:

### `npm install`

Then you can run with command

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
