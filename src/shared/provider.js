import React, {useState} from "react";
import {initialNotes} from "./initialValues";
import AppContext from "./context";
import {useHistory} from "react-router-dom";

const AppProvider = (props) => {
  const [notes, setNotes] = useState(initialNotes)

  const handleCreateNote = (newNote) => (event) => {
    event.preventDefault()
    setNotes([newNote, ...notes])
  }

  const handleDeleteNote = (index) => (event) => {
    event.preventDefault()
    setNotes(notes.filter((el , i) => i !== index))
  }

  return (<AppContext.Provider value={{
    notes: notes,
    handleCreateNote: handleCreateNote,
    handleDeleteNote: handleDeleteNote
  }}>
    {props.children}
  </AppContext.Provider>)
}

export default AppProvider