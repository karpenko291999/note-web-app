import MainPage from "../views/MainPage";
import NoteDetailPage from "../views/NoteDetailPage";

export const routes = [
  {
    path: '/note/:id',
    component: NoteDetailPage
  },
  {
    path: '/',
    component: MainPage
  }
]