import React from "react";
import {Box, Button, Container, Typography} from "@material-ui/core";
import './styles.css'

const FullNoteCard = ({title, note, handleDeleteNote}) => {
  return <Container>
    <Box display='flex' flexDirection='column' alignContent='center' mt='30px'>
      <Typography className='title' variant='h3' component='h3'>{title}</Typography>
      <Typography variant='body1'>{note}</Typography>
    </Box>
  </Container>
}

export default FullNoteCard