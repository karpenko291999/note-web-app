import React from "react";
import {Box, Button, Typography} from "@material-ui/core";
import deleteIcon from '../../../assets/icon-delete.png'
import './styles.css'

const ShortNoteCard = ({note, handleDeleteNote, handleCardClick}) => {
  return <div className='card'>
    <Box display='flex' justifyContent='space-between' alignItems='center' mb={3}>
      <Typography variant='h5' component='h5'>{note.title}</Typography>
      <Button variant='outlined' onClick={handleDeleteNote}>
        <img alt='Delete' src={deleteIcon} width='15px'/>
      </Button>
    </Box>
    <Typography onClick={handleCardClick} variant='body1'>{note.note}</Typography>
  </div>
}

export default ShortNoteCard