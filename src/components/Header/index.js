import React from 'react'
import './styles.css'
import logo from '../../assets/icon-logo.png'
import settings from '../../assets/icon-settings.png'
import {Box, Container} from "@material-ui/core";

const Header = () => {
  return <div className='header'>
    <Container>
      <Box display='flex' justifyContent='space-between' alignItems='center'>
        <img alt='Logo' src={logo} width='40px'/>
        <img alt='Settings' src={settings} width='30px'/>
      </Box>
    </Container>
  </div>
}

export default Header