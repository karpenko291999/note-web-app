import React from "react";
import {Box, Button, Container, InputBase, TextField, Typography} from "@material-ui/core";
import './styles.css'

const CreateNoteForm = ({handleFieldChange, handleCreateNote, newNote}) => {
  return <Container>
    <Box display='flex' justifyContent='center' my={5}>
        <Box display='flex' flexDirection='column' justifyContent='center' minWidth='250px' width='500px'>
          <Typography variant='h6'>Title</Typography>
          <InputBase placeholder='Enter title...'
                     className='textField'
                     required
                     value={newNote.title}
                     onChange={handleFieldChange('title')}/>
          <Typography variant='h6'>Note</Typography>
          <InputBase multiline
                     className='textField'
                     placeholder='Enter note...'
                     required
                     rows={5}
                     value={newNote.note}
                     onChange={handleFieldChange('note')}/>
          <Button variant='outlined' color='primary' onClick={handleCreateNote}>Create</Button>
        </Box>
    </Box>
  </Container>
}

export default CreateNoteForm