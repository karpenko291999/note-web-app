import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {routes} from "../../shared/routes";
import AppProvider from "../../shared/provider";
import Header from "../../components/Header";

function App() {
  return (<AppProvider>
      <Header/>
      <BrowserRouter>
        <Switch>
          {routes.map((route, index) => {
            return <Route key={index} path={route.path} component={route.component}/>
          })}
        </Switch>
      </BrowserRouter>
  </AppProvider>
  );
}

export default App;
