import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import CreateNoteForm from "../../components/CreateNoteForm";
import ShortNoteCard from "../../components/Cards/ShortNoteCard";
import {initialNewNote} from "../../shared/initialValues";
import AppContext from "../../shared/context";
import './styles.css'
import {Container} from "@material-ui/core";


const MainPage = () => {
  const [newNote, setNewNote] = useState(initialNewNote)

  const history = useHistory()

  const handleFieldChange = (type) => (event) => {
    setNewNote({...newNote, [type]: event.target.value})
  }

  const handleCardClick = (index) => () => {
    history.push(`/note/${index}`)
  }

  return (<AppContext.Consumer>
      {context => (
        <div className='container'>
          <CreateNoteForm handleFieldChange={handleFieldChange}
                          newNote={newNote}
                          handleCreateNote={context.handleCreateNote(newNote)}/>
          <Container>
            <div className='grid'>
              {context.notes?.map((note, index) => {
                return <ShortNoteCard key={index}
                                      note={note}
                                      handleCardClick={handleCardClick(index)}
                                      handleDeleteNote={context.handleDeleteNote(index)}/>
              })}
            </div>
          </Container>
        </div>
      )}
  </AppContext.Consumer>
  )
}

export default MainPage

