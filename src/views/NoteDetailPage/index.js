import React, {useEffect, useState} from 'react'
import AppContext from "../../shared/context";
import {useLocation} from "react-router-dom";
import FullNoteCard from "../../components/Cards/FullNoteCard";

const NoteDetailPage = () => {
  const [noteIndex, setNoteIndex] = useState()

  const location = useLocation()

  const getNoteIndex = () => {
    const index = location.pathname.split('/').slice(-1).join()
    setNoteIndex(index)
  }

  useEffect(() => {
    getNoteIndex()
  }, [])

  return <AppContext.Consumer>
    {context => (
      <div>
        <FullNoteCard title={context.notes[noteIndex]?.title}
                      handleDeleteNote={context.handleDeleteNote(noteIndex)}
                      note={context.notes[noteIndex]?.note}/>
      </div>
    )}
  </AppContext.Consumer>
}

export default NoteDetailPage